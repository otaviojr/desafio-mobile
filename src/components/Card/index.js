import React from 'react';
import {View, Image } from 'react-native';

export default (props) => {
  return (
    <View style={props.customStyle}>
      <Image
        source={{uri: props.url}}
        resizeMode={props.resiceMode}
        style={props.customStyle}
      />
    </View>
  );
};
