import React, { useState } from 'react';
import { SafeAreaView, ScrollView, View, Text } from 'react-native';

import styles from './styles';
import Card from '../Card';

const images = [
  'https://img1.wsimg.com/isteam/ip/04aacdd4-130d-4a57-924f-f067136251e2/smiling.jpg',
  'https://i.em.com.br/M0m1-rP0WaM0K7lnisB9h-nYqnQ=/790x/smart/imgsapp.em.com.br/app/noticia_127983242361/2021/06/16/1277548/20210616210231509250u.jpg',
  'https://englishlive.ef.com/pt-br/blog/wp-content/uploads/sites/16/2014/07/beagle-lindo.jpg'
 ];

export default () => {
  const [imageActive, setImageActive] = useState(0);
  
  const onchange = (nativeEvent) => {
    if(nativeEvent){
      const slide = Math.ceil(nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width);
      if(slide != imageActive){
        setImageActive(slide);
      }
    }
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.wrap}>
        <ScrollView
          onScroll={({nativeEvent}) => onchange(nativeEvent)}
          showsHorizontalScrollIndicator={false}
          pagingEnabled
          horizontal
          style={styles.wrap}
          >
          {
            images.map((e, index) => 
              <Card
                key={e}
                resizeMode="stretch"
                url={e}
                customStyle={styles.wrap}
              />
            ) 
          }
        </ScrollView>
        <View style={styles.wrapDot}>
          {
            images.map((e, index) => 
              <Text
                key={e}
                style={(imageActive == index) ? styles.dotActive : styles.dot}
              >
              {'\u2B24'}
              </Text>)
          }
        </View>
      </View>
    </SafeAreaView>
  );
};
