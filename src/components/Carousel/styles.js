import { StyleSheet, Dimensions } from "react-native";

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 35,
  },
  wrap: {
    width: WIDTH * 0.9,
    height: HEIGHT * 0.30,
    borderRadius: 8
  },
  wrapDot: {
    position: 'absolute',
    bottom: 0,
    flexDirection: 'row',
    alignSelf: 'center'
  },
  dotActive: {
    margin: 3,
    color: '#1793a6',
  },
  dot: {
    margin: 3,
    color: 'black'
  }
});

export default styles;
