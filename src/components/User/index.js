import React, { useEffect, useState } from 'react';
import {View, Text, Image} from 'react-native';

import Axios from '../../services/Axios';
import styles from './styles';

export default (props) => {
  const [image, setImage] = useState(null);
  const [name, setName] = useState('Name')
  const [location, setLocation] = useState('Location');
  
  useEffect(() => {
    Axios.get(props.userName)
      .then(res => {
        setImage(res.data.avatar_url);
        setName(res.data.name);
        setLocation(res.data.location)
      })
      .catch(error => {
        // console.error(error);
        setImage('https://cdn2.iconfinder.com/data/icons/delivery-and-logistic/64/Not_found_the_recipient-no_found-person-user-search-searching-4-512.png');
        setName('User not found');
        setLocation('Location');
      })
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.box}>
        <Image
          source={{uri: image}}
          style={styles.Icon}
        />
        <View style={styles.info}>
          <Text style={styles.TextInfo}>{name}</Text>
          <Text style={styles.TextInfo}>{location}</Text>
        </View>

      </View>
    </View>
  );
}
