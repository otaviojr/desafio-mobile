import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  box: {
    width: 200,
    height: 200,
    backgroundColor: '#1793a6',
    alignItems: 'center',
    borderRadius: 8
  },
  Icon: {
    marginTop: 25,
    borderRadius: 50,
    width: 100,
    height: 100
  },
  info: {
    marginTop: 20,
  },
  TextInfo: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center'
  }
});

export default styles;
