import React from 'react';
import { TouchableOpacity, Image, View, Text } from "react-native";

import { Ionicons } from '@expo/vector-icons';
import styles from './styles';

export default function Header(props) {
  return (
    <View style={styles.header}>
      <View>
        <Image source={require('../../assets/comp-logo.png')} style={styles.logo} />
      </View>

      <Text style={styles.textHeader}>{props.title}</Text>

      <TouchableOpacity>
        <Ionicons name="md-search-sharp" size={26} color="#1793a6" />
      </TouchableOpacity>
    </View>
  );
}
