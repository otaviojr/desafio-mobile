import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  header: {
    backgroundColor: 'black',
    borderBottomWidth: 1,
    borderBottomColor: '#1793a6',
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 16,
    height: 50,
    width: '100%'
  },  
  textHeader: {
    fontSize: 25,
    color: '#1793a6',
  },
  logo: {
    width: 30,
    height: 30,
    flex: 1,
    resizeMode: "contain",
  },
});

export default styles;
