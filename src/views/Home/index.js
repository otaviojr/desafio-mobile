import React from 'react';
import { ScrollView, StatusBar } from 'react-native';

import styles from './styles';

import Header from '../../components/Header/';
import Carousel from '../../components/Carousel';
import User from '../../components/User';

export default () => {
  return(
    <ScrollView contentContainerStyle={styles.container}>
      <StatusBar/>
      <Header title={"Início"}/>
      <Carousel/>
      <User userName='torvalds'/>
    </ScrollView>
  );
};
