import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
  flex: 1,
  alignItems: 'center',
  backgroundColor: '#1d1d22'
},
});

export default styles;